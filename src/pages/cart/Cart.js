import SectionHeader from "../../components/SectionHeader";
import CartItemList from "./CartItemList";
import Footer from "../../components/Footer";

const clearCart = () => {
	window.location.reload();
}

const Cart = ({items, removeFromCart}) => {
	if (items.length) {
		return (
			<>
				<div className="invisible" id="top"></div>
				<SectionHeader title="My Cart"/>
				<div className="w-full flex justify-end px-8 pb-4">
					<label htmlFor="my-modal-6" className="btn btn-primary">Proceed to Checkout</label>
				</div>
				<CartItemList items={items} removeFromCart={removeFromCart} />
				<Footer/>
				<input type="checkbox" id="my-modal-6" className="modal-toggle" />
				<div className="modal modal-bottom sm:modal-middle">
					<div className="modal-box">
						<h3 className="font-bold text-lg">Checkout</h3>
						<p className="py-4">Buying all of these items now</p>
						<div className="modal-action">
							<label htmlFor="my-modal-6" className="btn">Cancel</label>
							<label htmlFor="my-modal-6" className="btn btn-primary" onClick={() => clearCart()}>Ok</label>
						</div>
					</div>
				</div>
			</>
		);
	}
	else {
		return (
			<>
				<div className="invisible" id="top"></div>
				<SectionHeader title="My Cart"/>
				<CartItemList items={items} removeFromCart={removeFromCart} />
				<Footer/>
			</>
		);
	}

};

export default Cart;