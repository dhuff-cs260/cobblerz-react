const CartItem = ({item, removeFromCart}) => {
	return (
		<div className="card card-side bg-base-100 shadow-xl">
			<figure><img className="w-48 sm:w-60 h-72 overflow-hidden" src={item.imageURL} alt={item.title}/></figure>
			<div className="card-body">
				<h2 className="card-title">{item.title}</h2>
				<p>{item.description}</p>
				<p className="font-bold">${item.price}</p>
				<div className="card-actions justify-center sm:justify-end">
					<button className="btn btn-accent w-[101.48px]" onClick={removeFromCart.bind(this, item)}>
						Remove
					</button>
					<label htmlFor={`item-checkout-${item.title}`} className="btn btn-primary w-[101.48px]">Buy Now</label>
				</div>
			</div>
			<input type="checkbox" id={`item-checkout-${item.title}`} className="modal-toggle" />
			<div className="modal modal-bottom sm:modal-middle">
				<div className="modal-box">
					<h3 className="font-bold text-lg">Checkout</h3>
					<p className="py-4">Buying {item.title} now</p>
					<div className="modal-action">
						<label htmlFor={`item-checkout-${item.title}`} className="btn">Cancel</label>
						<label htmlFor={`item-checkout-${item.title}`} className="btn btn-primary" onClick={removeFromCart.bind(this, item)}>Ok</label>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CartItem;