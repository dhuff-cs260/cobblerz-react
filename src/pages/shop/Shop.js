import SectionHeader from "../../components/SectionHeader";
import ProductList from "../../components/ProductList";
import LifeStyle from "../../fakeData/Lifestyle"
import Athletic from "../../fakeData/Athletic"
import Formal from "../../fakeData/Formal"
import Footer from "../../components/Footer";
import {Hero} from "../../components/Hero";

const Shop = ({addToCart}) => {
	return (
		<>
			<div className="invisible" id="top"></div>
			<Hero
				style={{backgroundImage: 'url(' + require('../../assets/images/slide1.jpg') + ')'}}
				title="Shop The Hottest Styles"
				bodyText="Come and check out some of the hottest styles of the season!"
				actionText="Get Started" actionLink="/shop#Athletic"
			/>
			<SectionHeader title="Athletic"/>
			<ProductList products={Athletic()} addToCart={addToCart}/>
			<SectionHeader title="Lifestyle"/>
			<ProductList products={LifeStyle()} addToCart={addToCart}/>
			<SectionHeader title="Formal"/>
			<ProductList products={Formal()} addToCart={addToCart}/>
			<Footer/>
		</>
	);
};

export default Shop;