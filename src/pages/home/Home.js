import SectionHeader from "../../components/SectionHeader";
import ProductList from "../../components/ProductList";
import Featured from "../../fakeData/Featured";
import Footer from "../../components/Footer";
import {Hero} from "../../components/Hero";

const Home = ({addToCart}) => {
	const products = Featured();
	return (
		<>
			<div className="invisible" id="top"></div>
			<Hero
				style={{backgroundImage: 'url(' + require('../../assets/images/slide2.jpg') + ')'}}
				title="Cobblerz"
				bodyText="S-Tier cobbling services since 2022"
				actionText="Browse New Arrivals"
				actionLink="/#Featured"
			/>
			<SectionHeader title="Featured"/>
			<ProductList products={products} addToCart={addToCart}/>
			<Footer/>
		</>
	);

};

export default Home;