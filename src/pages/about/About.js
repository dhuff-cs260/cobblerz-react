import SectionHeader from "../../components/SectionHeader";
import QuestionList from "../../components/QuestionList";
import FAQData from "../../fakeData/faqData";
import ContactForm from "../../components/ContactForm";
import Footer from "../../components/Footer";

const About = () => {
	return (
		<>
			<div className="invisible" id="top"></div>
			<SectionHeader title="FAQ" />
			<QuestionList questionData={FAQData()}/>
			<SectionHeader title="Contact" id="contact"/>
			<ContactForm/>
			<Footer/>
		</>
	);
};

export default About;