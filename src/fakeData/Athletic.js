const Athletic = () => {
	return [
		{
			title: "Foam Runner",
			description: "The future of speed",
			price: 399.99,
			imageURL: require("../assets/images/foam.jpeg")
		},
		{
			title: "Karst",
			description: "A real head turner",
			price: 105.99,
			imageURL: require("../assets/images/karst.jpg")
		},
		{
			title: "Air Max",
			description: "Max air, max speed",
			price: 99.99,
			imageURL: require("../assets/images/max.webp")
		},
		{
			title: "SKODUL",
			description: "Extra bounce",
			price: 89.99,
			imageURL: require("../assets/images/skodiul.webp")
		},
		{
			title: "Champion",
			description: "There can only be one",
			price: 159.99,
			imageURL: require("../assets/images/champ.jpeg")
		},
		{
			title: "990v5 Core",
			description: "The classic's never fail",
			price: 139.99,
			imageURL: require("../assets/images/990.webp")
		},
		{
			title: "Barricade",
			description: "Unbreakable style",
			price: 129.99,
			imageURL: require("../assets/images/barricade.webp")
		},
		{
			title: "Vapor Lite",
			description: "A new way to fly around the court",
			price: 105.99,
			imageURL: require("../assets/images/vapor.jpg")
		}
	];
};

export default Athletic;