const Formal = () => {
	return [
		{
			title: "Peu Touring",
			description: "Ready for anywhere",
			price: 139.99,
			imageURL: require("../assets/images/peu.jpg")
		},
		{
			title: "Croc Lifted",
			description: "Groundbreaking twist on a classic style",
			price: 239.99,
			imageURL: require("../assets/images/crocs.webp")
		},
		{
			title: "Space",
			description: "Sleek, Mean, Beautiful",
			price: 339.99,
			imageURL: require("../assets/images/space.webp")
		},
		{
			title: "Trooper",
			description: "Winter is coming",
			price: 359.99,
			imageURL: require("../assets/images/trooper.webp")
		},
		{
			title: "Tux Wing Tip",
			description: "Every man needs a good tux",
			price: 279.99,
			imageURL: require("../assets/images/tux.jpeg")
		},
		{
			title: "Classic Wing Tip",
			description: "An unbeatable classic",
			price: 129.99,
			imageURL: require("../assets/images/brown.jpg")
		},
		{
			title: "Jack Eden",
			description: "High tops never looked so fine",
			price: 149.99,
			imageURL: require("../assets/images/jack.webp")
		},
		{
			title: "Sonoma",
			description: "Sleek new style on a classic shape",
			price: 139.99,
			imageURL: require("../assets/images/Sonoma.webp")
		},
	];
};

export default Formal;