const Featured = () => {
	return [
		{
			title: "Barricade",
			description: "Unbreakable style",
			price: 129.99,
			imageURL: require("../assets/images/barricade.webp")
		},
		{
			title: "Karst",
			description: "A real head turner",
			price: 105.99,
			imageURL: require("../assets/images/karst.jpg")
		},
		{
			title: "Air Max",
			description: "Max air, max speed",
			price: 99.99,
			imageURL: require("../assets/images/max.webp")
		},
		{
			title: "Drift",
			description: "Lift in drift",
			price: 89.99,
			imageURL: require("../assets/images/drift.jpg")
		},
		{
			title: "Traktori",
			description: "Splashing through puddles never looked so stylin'",
			price: 69.99,
			imageURL: require("../assets/images/traktori.jpeg")
		}
	];
};

export default Featured;