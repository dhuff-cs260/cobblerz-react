const LifeStyle = () => {
	return [
		{
			title: "Blazer",
			description: "New spin on an old classic",
			price: 99.99,
			imageURL: require("../assets/images/blazer.webp")
		},
		{
			title: "Morris Clown",
			description: "What's life without laughs",
			price: 79.99,
			imageURL: require("../assets/images/clown.webp")
		},
		{
			title: "Drift",
			description: "Lift in drift",
			price: 89.99,
			imageURL: require("../assets/images/drift.jpg")
		},
		{
			title: "Traktori",
			description: "Splashing through puddles never looked so stylin'",
			price: 69.99,
			imageURL: require("../assets/images/traktori.jpeg")
		},
		{
			title: "Air Force 1",
			description: "Ultimate air, Ultimate style",
			price: 129.99,
			imageURL: require("../assets/images/forces.jpg")
		},
		{
			title: "Air Jordan 1",
			description: "The ulitmate sneaker",
			price: 299.99,
			imageURL: require("../assets/images/jordan1.webp")
		},
		{
			title: "Dunk Retro",
			description: "A return of the iconic retro style",
			price: 219.99,
			imageURL: require("../assets/images/dunk.webp")
		},
		{
			title: "Yeezy 350",
			description: "Running never looked so good",
			price: 259.99,
			imageURL: require("../assets/images/350.webp")
		}
	];
};

export default LifeStyle;