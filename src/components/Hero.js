import { HashLink as Link } from "react-router-hash-link";

export const Hero = ({ style, title, bodyText, actionText, actionLink }) => {
	return (
		<div className="hero min-h-screen" style={style}>
			<div className="hero-overlay bg-opacity-60"></div>
			<div className="hero-content text-center text-neutral-content">
				<div className="max-w-md">
					<h1 className="mb-5 text-5xl font-bold">{title}</h1>
					<p className="mb-5">{bodyText}</p>
					<Link to={actionLink}><button className="btn btn-primary">{actionText}</button></Link>
				</div>
			</div>
		</div>
	);
}