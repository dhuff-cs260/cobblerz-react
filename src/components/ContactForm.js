const ContactForm = () => {
	return (
        <>
		  <div data-theme="forest" className="flex flex-col items-center">
		  	<div className="contact flex justify-center">
		  		<form action="#">
	      
	            	<label className="text-white" htmlFor="fname">Full Name</label>
	            	<input className="my-4" type="text" id="fname" name="firstname" placeholder="Your name..."/>
	      
	            	<label className="text-white my-2" htmlFor="lname">Email</label>
	            	<input className="my-4" type="text" id="lname" name="lastname" placeholder="Your Email..."/>
	      	
	            	<label className="text-white my-2" htmlFor="subject">Message</label>
	            	<textarea className="my-4" id="subject" name="subject" placeholder="Message..." style={{height: "200px"}}></textarea>
	      
	            	<input className="bg-primary" type="submit" value="Submit"/>
            	</form>
		  	</div>
          </div> 
        </>
	);
}

export default ContactForm;