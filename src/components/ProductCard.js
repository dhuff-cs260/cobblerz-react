const ProductCard = ({product, addToCart}) => {
	return (
		<div className="flex-none card card-compact w-80 bg-base-100 h-[32rem] shadow-xl">
			<figure className="overflow-hidden w-80">
				<img src={product.imageURL} alt={product.title} />
			</figure>
			<div className="card-body">
				<h2 className="card-title">{product.title}</h2>
				<p>{product.description}</p>
				<p className="font-bold">${product.price}</p>
				<div className="card-actions justify-end">
					<button className="btn btn-primary" onClick={addToCart.bind(this, product)}>Add to Cart</button>
				</div>
			</div>
		</div>
	);
}

export default ProductCard;