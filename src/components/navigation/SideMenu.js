import {HashLink as Link} from "react-router-hash-link";

const SideMenu = ({close}) => {
	return (
		<div className="drawer-side">
			<label htmlFor="nav-drawer" className="drawer-overlay"></label>
			<ul className="menu p-4 overflow-y-auto w-80 bg-base-100">
				<li onClick={close}><Link to="/#top">Home</Link></li>
				<li onClick={close}><Link to="/shop#top">Shop</Link></li>
				<li onClick={close}><Link to="/about#top">About</Link></li>
			</ul>
		</div>
	);
};

export default SideMenu;