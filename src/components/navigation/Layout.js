import { Outlet } from "react-router-dom";
import { useState } from 'react';
import SideMenu from "./SideMenu";
import TopMenu from "./TopMenu";

const Layout = ({cartItems}) => {
	const [drawerOpen, setDrawerOpen] = useState(false);
	return (
		<div className="drawer">
			<input id="nav-drawer"
				   type="checkbox"
				   className="drawer-toggle"
				   checked={drawerOpen}
				   onChange={() => setDrawerOpen(!drawerOpen)}
			/>
			<div className="drawer-content flex flex-col">
				<TopMenu cartItems={cartItems}/>
				<Outlet/>
			</div>
			<SideMenu close={() => setDrawerOpen(false)}/>
		</div>
	);
};

export default Layout;