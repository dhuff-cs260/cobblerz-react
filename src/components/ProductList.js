import ProductCard from "./ProductCard";

const ProductList = ({products, addToCart}) => {
	const items = products.map((product, index) => <ProductCard key={index} product={product} addToCart={addToCart}/>);
	return (
		<div className="productList justify-center">{items}</div>
	)
};

export default ProductList;