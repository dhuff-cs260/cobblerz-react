/** a title a section of content on a page */
const SectionHeader = ({ title}) => {
	return <h1 id={title} className="text-3xl font-bold flex justify-center py-8">{title}</h1>;
}

export default SectionHeader;