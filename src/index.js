import ReactDOM from "react-dom/client";
import {useState} from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import Layout from "./components/navigation/Layout";
import Home from "./pages/home/Home";
import Shop from "./pages/shop/Shop";
import About from "./pages/about/About";
import Cart from "./pages/cart/Cart";

export default function App() {
    const [cartItems, setCartItems] = useState([]);
    const addToCart = (item) => {setCartItems([...cartItems, item])};
    const removeFromCart = (item) => {setCartItems([...cartItems].filter(cartItem => cartItem !== item))};
    return (
        <HashRouter>
            <Routes>
                <Route path="/" element={<Layout cartItems={cartItems} />}>
                    <Route index element={<Home addToCart={addToCart} />} />
                    <Route path="shop" element={<Shop addToCart={addToCart} />} />
                    <Route path="about" element={<About />} />
                    <Route path="cart" element={<Cart items={cartItems} removeFromCart={removeFromCart} />} />
                </Route>
            </Routes>
        </HashRouter>
    );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
